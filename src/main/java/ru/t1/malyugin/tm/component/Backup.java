package ru.t1.malyugin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.command.data.load.DataBackupLoadCommand;
import ru.t1.malyugin.tm.command.data.save.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    private void load() {
        @NotNull final String backupDir = bootstrap.getPropertyService().getApplicationDumpDir();
        @NotNull final String backupFile = AbstractDataCommand.FILE_BACKUP;
        if (!Files.exists(Paths.get(backupDir + backupFile))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

}