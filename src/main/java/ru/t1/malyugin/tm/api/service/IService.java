package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    void clear();

    int getSize();

    @Nullable
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    M remove(@Nullable M model);

    @NotNull
    M removeById(@Nullable String id);

    @NotNull
    M removeByIndex(@Nullable Integer index);

    void removeAll(@Nullable List<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

}